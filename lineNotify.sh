#!/bin/sh

export line_token="Al8iA6DDxQKgNjMKLrJiSD9aGVsy4RywawVYgTMBDHa"

function notify ()
{
    curl -X POST -H "Authorization: Bearer $line_token" -F "message=$*" https://notify-api.line.me/api/notify
    echo "Send Notify $*"
}