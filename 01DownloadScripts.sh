#!/bin/sh
# sudo yum install git 
# wget https://gitlab.com/certscript/aws3k/-/raw/main/01DownloadScripts.sh | sh 
# wget https://gitlab.com/certscript/aws3k/-/raw/main/02Docker_03GETInstallation.sh
if [ -d $HOME/aws3k ] ; then 
    if [ -d $HOME/aws3k/.git ] ; then 
        cd $HOME/aws3k && git pull
    else
        echo "Not a git repo"
        git clone git@gitlab.com:certscript/aws3k.git
        echo "AWS3K Repo cloned."
    fi
else
    # Always Sync the latest version script.
    echo "Manual wget script files."
    mkdir -p $HOME/aws3k && cd $HOME/aws3k
    wget https://gitlab.com/certscript/aws3k/-/raw/main/04TerraformConfiguration.sh
    wget https://gitlab.com/certscript/aws3k/-/raw/main/05AnsibleConfiguration.sh
    wget https://gitlab.com/certscript/aws3k/-/raw/main/06DockerRunTFA.sh
    wget https://gitlab.com/certscript/aws3k/-/raw/main/07TestDeplyment.sh
    wget https://gitlab.com/certscript/aws3k/-/raw/main/08TerraformDestroy.sh
    wget https://gitlab.com/certscript/aws3k/-/raw/main/09Remain14StepToFIN.sh
fi

# check if AWS Vars were ready
# AWS_prefix is set for group EC2 instance
# export AWS_region="eu-central-1" # Frankfurt
# export AWS_region="us-west-1" # N. Calafornia
# export AWS_region="us-west-2" # Oregon
# export AWS_region="ap-northeast-1" # Tokyo
# Create elastic IP first 

if [ ! -f ./AWS.VAR ] ; then
cat << AWS_VAR > ./AWS.VAR
    export AWS_ACCESS_KEY_ID='AKIAXR7WGANQEYQTENFA'
    export AWS_SECRET_ACCESS_KEY='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'
    export GITLAB_PASSWORD='A21001616@'
    export AWS_prefix='gitlab-psecert-andrew'
    export AWS_external_ip_allocation='eipalloc-0f33fb32d0a3dda04'
    export AWS_elastic_ip='35.78.162.115'
AWS_VAR

# Get Region info by EC2 Console region.
    EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
    export AWS_REGION="`echo \"$EC2_AVAIL_ZONE\" | sed 's/[a-z]$//'`"
    echo "export AWS_REGION=$AWS_REGION" >> ./AWS.VAR
fi

echo "============ Basic Configuration: AWS Variables ================================"
cat ./AWS.VAR
read -p "============ if AWS Variables is not you think, Ctrl+C now .===================="
echo "Fine, We Go on."
source ./AWS.VAR
source ./lineNotify.sh

# Call 04-05 in sequenced; 02-03 has been manual done first.
# echo "============ Basic Install: Docker & GET ======================================="
# sh ./02Docker_03GETInstallation.sh
export GET_PATH="/home/$USER/gitlab-environment-toolkit"
echo "============ $GET_PATH ======="
echo "============ Basic Configuration: TerraForm ===================================="
eval sh ./04TerraformConfiguration.sh

#[ $# -eq 0 ] && 
echo "============ Basic Configuration: Ansible ======================================"
eval sh ./05AnsibleConfiguration.sh
notify "TerraForm & ANsible Configurated, Start to run $AWS_elastic_ip building"
#[ $# -eq 0 ] && 
read -p "============ RUN: TerraForm & Ansible =========================== PRESS ENTER =="
sh ./06DockerRunTFA.sh
notify "TerraForm & ANsible Done, Access the GitLab application using http://$AWS_elastic_ip and root:$GITLAB_PASSWORD"

[ $# -eq 0 ] && echo "============ TEST: Gitlab Deployment ==========================================="
echo "============ Only print out test item. ========================== PRESS ENTER =="
sh ./07TestDeplyment.sh

exit

[ $# -eq 0 ] && read -s -t 5 -p "============ FIN1: TerraForm Destroy ============================ PRESS ENTER =="
sh ./08TerraformDestroy.sh
[ $# -eq 0 ] && read -s -t 5 -p "============ FIN2: Move to AWS Cloud Native SaaS================= PRESS ENTER =="
sh ./09Remain14StepToFIN.sh