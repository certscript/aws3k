#!/bin/sh
export A_I_PATH=$GET_PATH/ansible/environments/3k/inventory
mkdir -p $A_I_PATH
cd $A_I_PATH
touch 3k.aws_ec2.yml vars.yml

cat << 3K.AWS_EC2.YML > $A_I_PATH/3k.aws_ec2.yml
plugin: aws_ec2
regions:
  - $AWS_REGION
filters:
  tag:gitlab_node_prefix: $AWS_prefix # Same prefix set in Terraform
keyed_groups:
  - key: tags.gitlab_node_type
    separator: ''
  - key: tags.gitlab_node_level
    separator: ''
hostnames:
  - tag:Name
compose:
  ansible_host: public_ip_address
3K.AWS_EC2.YML

cat << VAR.YML > $A_I_PATH/vars.yml
all:
  vars:
    # Ansible Settings
    ansible_user: ubuntu
    ansible_ssh_private_key_file: "../keys/id_rsa"
    cloud_provider: "aws"

    aws_region: "$AWS_region"

    # General Settings
    prefix: "$AWS_prefix"
    external_url: "http://$AWS_elastic_ip"
    # gitlab_license_file: "../../../sensitive/GitLabBV.gitlab-license"

    # Component Settings
    patroni_remove_data_directory_on_rewind_failure: false
    patroni_remove_data_directory_on_diverged_timelines: false

    # Passwords / Secrets
    gitlab_root_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    grafana_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    postgres_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    patroni_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    consul_database_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    gitaly_token: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    pgbouncer_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    redis_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    praefect_external_token: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    praefect_internal_token: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
    praefect_postgres_password: "{{ lookup('env', 'GITLAB_PASSWORD') }}"
VAR.YML
echo " "
ls -la $A_I_PATH
echo "################### Check ansible/environments/3k/inventory ####################" 
# Configure Ansible output logging
echo ""
echo "##### Replace $GET_PATH/ansible/ansible.cfg ####################################"
cp $HOME/aws3k/ready.conf/ansible.cfg $GET_PATH/ansible/ansible.cfg
# sudo /usr/bin/sed -i '2a display_args_to_stdout = True' $GET_PATH/ansible/ansible.cfg
# sudo /usr/bin/sed -i '2a log_path = /gitlab-environment-toolkit/ansible/environments/3k/inventory/ansible.log' $GET_PATH/ansible/ansible.cfg
echo "################### Check ansible/ansible.cfg ##################################"
sudo head $GET_PATH/ansible/ansible.cfg