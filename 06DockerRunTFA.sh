#!/bin/sh

# Run a Toolkit Docker Container in the interactive passing the environment variables and mounting the required volume folders
docker run -t -d \
--name 3Ksite \
-v /home/ec2-user/gitlab-environment-toolkit/keys:/gitlab-environment-toolkit/keys \
-v /home/ec2-user/gitlab-environment-toolkit/ansible/environments/3k:/gitlab-environment-toolkit/ansible/environments/3k \
-v /home/ec2-user/gitlab-environment-toolkit/ansible/ansible.cfg:/gitlab-environment-toolkit/ansible/ansible.cfg \
-v /home/ec2-user/gitlab-environment-toolkit/terraform/environments/3k:/gitlab-environment-toolkit/terraform/environments/3k \
-e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
-e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
-e GITLAB_PASSWORD=$GITLAB_PASSWORD \
registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:latest
echo "################## Check GET docker status #####################################"
sudo docker ps 
echo "################## Check GET docker status #####################################"
echo ""
echo "============= Terrafrom Init & Plan now ========================================"

cat << TERRAFORM_PLAN > terraform_plan.sh
#!/bin/sh
cd /gitlab-environment-toolkit/terraform/environments/3k 
terraform init -input=false
terraform plan -out 3k.aws_ec2.tfplan -input=false
TERRAFORM_PLAN

docker cp ./terraform_plan.sh 3Ksite:/root/
docker exec -it 3Ksite chmod +x /root/terraform_plan.sh
docker exec -it 3Ksite /root/terraform_plan.sh

echo "============= Terrafrom Apply now =============================================="
read -p "============ If You are not clear about what you're doing, Ctrl+C leave now. ==="
cat << TERRAFORM_APLY > terraform_aply.sh
#!/bin/sh
cd /gitlab-environment-toolkit/terraform/environments/3k 
terraform apply -auto-approve 3k.aws_ec2.tfplan
TERRAFORM_APLY

docker cp ./terraform_aply.sh 3Ksite:/root/
docker exec -it 3Ksite chmod +x /root/terraform_aply.sh
docker exec -it 3Ksite /root/terraform_aply.sh
echo "###################### Check AWS Console if VM were running ####################"
echo "============ If numbers of instance were < 30 , Press Ctrl+C leave now. ========"
read -p "============ RUN: Ansible list now ============================== PRESS ENTER =="
echo "============= Ansible List Host now ============================================"
cat << ANSIBLE_LIST > ansible_list.sh
#!/bin/sh
cd /gitlab-environment-toolkit/ansible
ansible all -m ping -i environments/3k/inventory --list-host
ANSIBLE_LIST

docker cp ./ansible_list.sh 3Ksite:/root/
docker exec -it 3Ksite chmod +x /root/ansible_list.sh
docker exec -it 3Ksite /root/ansible_list.sh
read -p "============ If numbers of instance were < 30 , Press Ctrl+C leave now. ========"

cat << 'ANSIBLE_PLAY' > ansible_play.sh
#!/bin/sh
play_start=`date +%s`
cd /gitlab-environment-toolkit/ansible
ansible-playbook -i environments/3k/inventory playbooks/all.yml
echo "============ RUN: Ansible PlayBook now ========================== PRESS ENTER =="
play_end=`date +%s`
runtime=$((play_end-play_start))
echo "################## Ansible Play time: $runtime Secs #####################"
msg="Played$runtimeSecs"
curl -X POST -H "Authorization: Bearer Al8iA6DDxQKgNjMKLrJiSD9aGVsy4RywawVYgTMBDHa" -F "message=$msg" https://notify-api.line.me/api/notify
ANSIBLE_PLAY

docker cp ./ansible_play.sh 3Ksite:/root/
docker exec -it 3Ksite chmod +x /root/ansible_play.sh
read -p  "==== PRESS ENTER  : docker exec -it 3Ksite /root/ansible_play.sh = PRESS ENTER ="
docker exec -it 3Ksite /root/ansible_play.sh