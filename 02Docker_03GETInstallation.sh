#!/bin/sh
echo "=================== Step 02 Install Docker Package & GET ======================="
sudo yum update -y
sudo yum install -y docker
sudo systemctl enable docker
sudo systemctl start docker
sudo usermod -aG docker ec2-user
sudo yum install -y git
echo "=================== Docker installed for ec2-user =============================="

echo "=================== check docker status ========================================"
sudo chmod 666 /var/run/docker.sock # 先做先贏
sudo docker -version && sudo docker ps

# Step 03
# to clone GET
echo "===================Step 03  Git clone Gitlab Enviroment Toolkit ================"
cd /home/ec2-user
git clone https://gitlab.com/gitlab-org/gitlab-environment-toolkit.git 
echo "=================== GET cloned. ================================================"

echo "=================== Pull the Toolkit's image ==================================="
sudo docker pull registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:latest
echo "=================== Docker image of Toolkit pulled. ============================"

# leaving the passphrase empty, and completing the ssh key creation 
# these will be used on the gitlab instance
cd gitlab-environment-toolkit
echo "=================== Create a ssh rsa key pair for GET... ======================="
ssh-keygen -q -t rsa -b 2048 -f keys/id_rsa  -P ""
echo "=================== SSH key pair generated. Ready to go.========================"