#!/bin/sh
# 1. Replacing Omnibus Components for AWS managed Services
# edit gitlab-environment-toolkit/terraform/environments/3k/variables.tf including Elasticache and RDS passwords:
cat << Elasticache_RDS_PassWD > gitlab-environment-toolkit/terraform/environments/3k/variables.tf
variable "elasticache_redis_password" {
    type = string
}

variable "rds_postgres_password" {
    type = string
}
Elasticache_RDS_PassWD

# 2. Create gitlab-environment-toolkit/terraform/environments/3k/outputs.tf files to access the internal module outputs to retrive Elasticache, RDS and ILB details:
cat << TF_Elasticache_RDS_OUTPUT > gitlab-environment-toolkit/terraform/environments/3k/outputs.tf
output "rds_postgres_connection" {
    value = try(module.gitlab_ref_arch_aws.rds_postgres_connection, [])
}

output "elasticache_redis_persistent_connection" {
    value = try(module.gitlab_ref_arch_aws.elasticache_redis_connection, [])
}

output "gitlab_internal_load_balancer_dns" {
  value = try(module.gitlab_ref_arch_aws.elb_internal_host, [])
}
TF_Elasticache_RDS_OUTPUT

# 3. Edit gitlab-environment-toolkit/terraform/environments/3k/environment.tf adding GET required entries to provision Elasticache and RDS and deploy and Internal Load Balancer on ELB.
cat << PROVISION_ELB >> gitlab-environment-toolkit/terraform/environments/3k/environment.tf
# ILB 
elb_internal_create = true

# RDS
rds_postgres_instance_type = "m5.2xlarge"
rds_postgres_password = var.rds_postgres_password

# Elasticcache
elasticache_redis_node_count = 2
elasticache_redis_instance_type = "m5.large"
elasticache_redis_password = var.elasticache_redis_password
PROVISION_ELB

# 4. Before running Terraform we need to export environment variables elasticache_redis_password and rds_postgres_password we can use the same password already set on $GITLAB_PASSWORD
export TF_VAR_rds_postgres_password=$GITLAB_PASSWORD
export TF_VAR_elasticache_redis_password=$GITLAB_PASSWORD

# 5. Run a Toolkit Docker Container adding the new variables. The command should look like following:
docker run -it  \
--name 3Ksite \
-v /home/ec2-user/gitlab-environment-toolkit/keys:/gitlab-environment-toolkit/keys \
-v /home/ec2-user/gitlab-environment-toolkit/ansible/environments/3k:/gitlab-environment-toolkit/ansible/environments/3k \
-v /home/ec2-user/gitlab-environment-toolkit/terraform/environments/3k:/gitlab-environment-toolkit/terraform/environments/3k \
-e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
-e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
-e GITLAB_PASSWORD=$GITLAB_PASSWORD \
-e TF_VAR_rds_postgres_password=$GITLAB_PASSWORD \
-e TF_VAR_elasticache_redis_password=$GITLAB_PASSWORD \
registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:latest

# 6. Run and  configure GitLab to use the RDS, Elasticache, and the ELB.
echo "This should add 20 new resources, and usually take 8-10 minutes"
docker exec 3Ksite "cd /gitlab-environment-toolkit/terraform/environments/3k/ && terraform apply" 
echo "Copy the output and poste as a comment on this issue"
docker exec 3Ksite "cd /gitlab-environment-toolkit/terraform/environments/3k/ && terraform output"

# 7. Edit /home/ec2-user/gitlab-environment-toolkit/ansible/environments/3k/inventory/vars.yml
cat << Elasticache_RDS_ELV_VAR >>/home/ec2-user/gitlab-environment-toolkit/ansible/environments/3k/inventory/vars.yml
postgres_external: true
internal_lb_host: "$internal_lb_host"
redis_host: "$redis_host"
postgres_host: "$postgres_host"
Elasticache_RDS_ELV_VAR

# 8. Inside the container let's run the Ansible scripts
echo "Test connection with hosts"
docker exec 3Ksite "cd /gitlab-environment-toolkit/ansible && ansible all -m ping -i environments/3k/inventory --list-hosts"
echo "Inside the container, start ansible playbook"
docker exec 3Ksite "cd /gitlab-environment-toolkit/ansible && ansible-playbook -i environments/3k/inventory/ playbooks/all.yml"
echo "Manual fix of the task Get Omnibus Postgres Primary gitlab-org/gitlab-environment-toolkit!555 (merged) when running GET older than 2.0.1"

# 9. Access a rails node and confirm that is using the RDS and Elasticache in the terminal run:
ssh -i /home/ec2-user/gitlab-environment-toolkit/keys/id_rsa ubuntu@$your_rails_public_dns
sudo vim /etc/gitlab/gitlab.rb
# Check the configurations: gitlab_rails['db_host'], gitlab_rails['redis_host'] and gitaly_address. They must be pointing for postgres_host, redis_host and internal_lb_host provide on step 7

# 10. Test the GitLab application using http://<your_elastic_ip> the same information provided on external_url in the vars.yml. With user root and the password as set in the variable GITLAB_PASSWORD. If everything is working as expected move to the next step.

# 11. Remove the Redis, Postgres, Pgbouncer and HAProxy Internal editing /home/ec2-user/gitlab-environment-toolkit/terraform/environments/3k/environment.tf and commenting/removing the lines below:
    # redis_node_count = 3
    # redis_instance_type = "m5.large"

    # postgres_node_count = 3
    # postgres_instance_type = "m5.large"

    # pgbouncer_node_count = 3
    # pgbouncer_instance_type = "c5.large"

    # haproxy_internal_node_count = 1
    # haproxy_internal_instance_type = "c5.large"

# 12. Run the Toolkit's container running the command below:
docker run -it  \
--name 3Ksite \
-v /home/ec2-user/gitlab-environment-toolkit/keys:/gitlab-environment-toolkit/keys \
-v /home/ec2-user/gitlab-environment-toolkit/ansible/environments/3k:/gitlab-environment-toolkit/ansible/environments/3k \
-v /home/ec2-user/gitlab-environment-toolkit/terraform/environments/3k:/gitlab-environment-toolkit/terraform/environments/3k \
-e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
-e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
-e GITLAB_PASSWORD=$GITLAB_PASSWORD \
-e TF_VAR_rds_postgres_password=$GITLAB_PASSWORD \
-e TF_VAR_elasticache_redis_password=$GITLAB_PASSWORD \
registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:latest

# 13.  Inside the container run the commands that follow:
docker exec 3Ksite "cd /gitlab-environment-toolkit/terraform/environments/3k && terraform apply"

 echo "This should not affect the application, since the components are not being used anymore."
 echo " Once you are done with the environment dont fortget to remove all the deployed resources on AWS,"
 echo " going over the next and final step."

# 14. Inside container the container once you are done with your environment don't forget to tear it down.
docker exec 3Ksite "cd /gitlab-environment-toolkit/terraform/environments/3k && terraform destroy"
