#!/bin/sh

# Configure GET to deploy the 3K reference architecture
TF_PATH=$GET_PATH/terraform/environments/3k
mkdir -p $TF_PATH
cd $TF_PATH
touch variables.tf main.tf environment.tf 

cat << ENVIROMENT.TF > $TF_PATH/environment.tf
# For the 3K Architecture
module "gitlab_ref_arch_aws" {
    source = "../../modules/gitlab_ref_arch_aws"

    prefix              = var.prefix
    ssh_public_key_file = file(var.ssh_public_key_file)

    # using network mode create with 3 subnets
    create_network = true
    subnet_pub_count = 3

    # External load balancer node
    haproxy_external_node_count = 1
    haproxy_external_instance_type = "c5.large"

    # Redis
    redis_node_count = 3
    redis_instance_type = "m5.large"
    
    # Consul + Sentinel
    consul_node_count = 3
    consul_instance_type = "c5.large"

    # Postgres
    postgres_node_count = 3
    postgres_instance_type = "m5.large"

    # Pgbouncer
    pgbouncer_node_count = 3
    pgbouncer_instance_type = "c5.large"

    # External Load Balancer
    haproxy_external_elastic_ip_allocation_ids = [var.external_ip_allocation]
    
    # Internal Load balancer node
    haproxy_internal_node_count = 1
    haproxy_internal_instance_type = "c5.large"
    
    # gitaly
    gitaly_node_count = 3
    gitaly_instance_type = "m5.xlarge"
    
    # praefect
    praefect_node_count = 3
    praefect_instance_type = "c5.large"

    # praefect postgres
    praefect_postgres_node_count = 1
    praefect_postgres_instance_type = "c5.large"

    # nfs
    gitlab_nfs_node_count = 1
    gitlab_nfs_instance_type = "c5.xlarge"

    #rails   
    gitlab_rails_node_count = 3
    gitlab_rails_instance_type = "c5.2xlarge"
    
    #grafana and prometheus
    monitor_node_count = 1
    monitor_instance_type = "c5.large"
    
    #sidekiq
    sidekiq_node_count = 4
    sidekiq_instance_type = "m5.large"
}
ENVIROMENT.TF
cat << VARIABLES.TF > $TF_PATH/variables.tf
variable "prefix" {
    default = "$AWS_prefix"
}

variable "region" {
    default = "$AWS_REGION"
}

variable "ssh_public_key_file" {
    default = "../../../keys/id_rsa.pub"
}

# This can be found in the Elastic IPs section
variable "external_ip_allocation" {
    default = "$AWS_external_ip_allocation" 
}

VARIABLES.TF

cat << MAIN.TF > $TF_PATH/main.tf
terraform {
    required_providers {
        aws = {
        source = "hashicorp/aws"
        version = "4.22.0"    
        }
    }
}

# Configure the AWS Provider
provider "aws" {
    region = var.region
}
MAIN.TF

ls -la $TF_PATH
echo "################# Check terraform/environments/3k above ########################"