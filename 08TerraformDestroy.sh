#!/bin/sh

cat << TERRAFORM_DESTROY > terraform_destroy.sh
#!/bin/sh
cd /gitlab-environment-toolkit/terraform/environments/3k 
terraform destroy
TERRAFORM_DESTROY

docker cp ./terraform_destroy.sh 3Ksite:/root/
docker exec -it 3Ksite chmod +x /root/terraform_destroy.sh
docker exec -it 3Ksite /root/terraform_destroy.sh

echo "================================================================================"
echo "To terminate the GET instance, through the AWS EC2 console right click it, Stop instance and then Terminate instance."
echo "================================================================================"
echo "[ ]Create an issue to follow how to replace and use cloud managed services like RDS and ElastiCache with GET."
echo "================================================================================"