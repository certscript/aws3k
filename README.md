# AWS3K

I cannot explain. Read the script inside

## Getting started

1. 進入VM後下載第一個腳本
    <pre><code> wget https://gitlab.com/certscript/aws3k/-/raw/main/02Docker_03GETInstallation.sh </code></pre>
1. 執行腳本
     <pre><code> sh 02Docker_03GETInstallation.sh </code></pre>
1. 下載完整project,注意需要先匯入金鑰，否則
     <pre><code> git clone git@gitlab.com:certscript/aws3k.git </code></pre>
     <pre><code> git clone https://gitlab.com/certscript/aws3k.git </code></pre>
1. 更改/再次確認01DownloadScripts.sh腳本內AWS環境變數
1. 執行腳本
    <pre><code> cd aws3k </code></pre>
    <pre><code> sh ./01DownloadScripts.sh </code></pre>

## 注意事項
1. 02Docker_03GETInstallation.sh 可重複執行
1. 04TerraformConfiguration.sh 、 05AnsibleConfiguration.sh僅設定參數，也可單獨執行
1. GET套件Terraform、Ansible個別階段指令稿複製進docker(name 3Ksite) /root/路徑下，可以用指令個別驅動，每次執行就會重新產生後替換掉，不要直接修改檔案內容
     <pre><code> docker exec -it 3Ksite /root/terraform_plan.sh </code></pre>
     <pre><code> docker exec -it 3Ksite /root/terraform_aply.sh </code></pre>
     <pre><code> docker exec -it 3Ksite /root/ansible_list.sh </code></pre>
     <pre><code> docker exec -it 3Ksite /root/ansible_play.sh </code></pre>
1. terraform apply 後要確認增加30個instances