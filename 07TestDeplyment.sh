#!/bin/sh

# Back to the instance terminal lets ssh in a rails node to check gitlab status and puma logs.
echo "Running something similar to ssh -i /home/ec2-user/gitlab-environment-toolkit/keys/id_rsa ubuntu@$your-rails-node-public-dns"
# 
sudo gitlab-ctl status
sudo gitlab-ctl tail puma
echo "Access the GitLab application using http://$AWS_elastic_ip"
echo "[ ]Login with user root and the password as set in the variable $GITLAB_PASSWORD"
echo "[ ]Create an issue in a new project, including the default README.md file"
echo "[ ]Create a merge request (MR) from the issue and change the project's README.md by adding as title Congratulations! You deployed GitLab HA with GET"
echo "[ ]Commit change and merge the MR"
echo "================================================================================"
echo "[ ]!!Provide screenshots of your AWS console showing the various compute nodes that were provisioned during this workshop"
echo "================================================================================"
echo "[ ]!!Provide screenshots of the running instance of GitLab (include the url bar with matching IP address to your elastic ip from initial steps) as a comment in this issue."
echo "================================================================================"
echo "[ ]Add the label `workshop::needs-review` to have someone from GitLab PS grade your workshop issue"
echo "================================================================================"